package com.example.bdubs.bitbuckettest;

import android.annotation.TargetApi;
import android.content.Intent;
import android.icu.util.Calendar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

public class RegisterNewUser extends AppCompatActivity {

    public EditText username;
    public EditText password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_new_user);
        final EditText date;
        date = (EditText)findViewById(R.id.DOB);
        TextWatcher tw = new TextWatcher() {
            private String current = "";
            private String mmddyyyy = "MMDDYYYY";
            private String ddmmyyyy = "DDMMYYYY";
            //@TargetApi(24)
            private Calendar cal = Calendar.getInstance(); @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(current)) {
                    String clean = s.toString().replaceAll("[^\\d.]", "");
                    String cleanC = current.replaceAll("[^\\d.]", "");

                    int cl = clean.length();
                    int sel = cl;
                    for (int i = 2; i <= cl && i < 6; i += 2) {
                        sel++;
                    }
                    //Fix for pressing delete next to a forward slash
                    if (clean.equals(cleanC)) sel--;

                    if (clean.length() < 8){
                        clean = clean + mmddyyyy.substring(clean.length());
                    }else{
                        //This part makes sure that when we finish entering numbers
                        //the date is correct, fixing it otherwise
                        int day  = Integer.parseInt(clean.substring(0,2));
                        int mon  = Integer.parseInt(clean.substring(2,4));
                        int year = Integer.parseInt(clean.substring(4,8));

                        if(mon > 12) mon = 12;
                        cal.set(Calendar.MONTH, mon-1);
                        year = (year<1900)?1900:(year>2100)?2100:year;
                        cal.set(Calendar.YEAR, year);
                        // ^ first set year for the line below to work correctly
                        //with leap years - otherwise, date e.g. 29/02/2012
                        //would be automatically corrected to 28/02/2012

                        day = (day > cal.getActualMaximum(Calendar.DATE))? cal.getActualMaximum(Calendar.DATE):day;
                        clean = String.format("%02d%02d%02d",mon, day, year);
                    }

                    clean = String.format("%s/%s/%s", clean.substring(0, 2),
                            clean.substring(2, 4),
                            clean.substring(4, 8));

                    sel = sel < 0 ? 0 : sel;
                    current = clean;
                    date.setText(current);
                    date.setSelection(sel < current.length() ? sel : current.length());
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        };
        date.addTextChangedListener(tw);
    }
    public void Register(View v){
        Intent intent = new Intent(this, Tutorial.class);
       // if(checkCredentials(findViewById(R.id.firstname),findViewById(R.id.lastname),findViewById(R.id.city),findViewById(R.id.country),findViewById(R.id.requested_username),findViewById(R.id.password),findViewById(R.id.confirm_password),))
        startActivity(intent);

    }
    public boolean checkCredentials(String fname,String lname,String city, String country, String hospital, String requsername, String password, String cpassword){
        /**
         *
         * CHECK AND VALIDATE INPUT SHIT HERE!!!!!
         *
         */

        return true;
    }
}
