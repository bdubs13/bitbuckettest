package com.example.bdubs.bitbuckettest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class Login extends AppCompatActivity implements View.OnClickListener {

    public EditText username;
    public EditText password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        username=(EditText)findViewById(R.id.username);
        password=(EditText)findViewById(R.id.password);

        username.setOnClickListener(this);
        password.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        ((EditText)findViewById(v.getId())).setText("");
    }
    public void Submit(View v){
        Intent intent = new Intent(this, MainMenu.class);
        //Take username and password from the text fields and pass it to the MainMenu
        //This is the nurses Login information
        EditText UN = (EditText) findViewById(R.id.username);
        EditText PW = (EditText) findViewById(R.id.password);
        String un = UN.getText().toString();
        String pw = PW.getText().toString();
        intent.putExtra(MainActivity.USERNAME_MESSAGE, un);
        intent.putExtra(MainActivity.PASSWORD_MESSAGE, pw);
        if(checkCredentials(un,pw)==true) {
            startActivity(intent);
        }else{

            /**
             * Notify User of invalid credentials
             */
        }

    }
    public void Register(View v){
        Intent intent = new Intent(this, RegisterNewUser.class);
        startActivity(intent);

    }
    private boolean checkCredentials(String username, String password){

        /****************************************************************
         *
         * ADD AWS QUERY OF NURSE/HW's CREDENTIALS HERE!!!!!
         *
         *                     IMPORTANT
         *
         ****************************************************************
         */


        return true;
    }
}
