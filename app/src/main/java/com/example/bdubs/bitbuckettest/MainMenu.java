package com.example.bdubs.bitbuckettest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        String username=getIntent().getExtras().getString(MainActivity.USERNAME_MESSAGE);
        TextView welcome=(TextView)findViewById(R.id.welcome_user);
        welcome.setText("Welcome to the main menu, "+username+"!\n What would you like to do today?");
    }
    public void New_Patient(View v){
        Intent intent = new Intent(this, NewPatient.class);
        startActivity(intent);
    }
    public void Existing_Patient(View v){
        Intent intent = new Intent(this, ExistingPatient.class);
        startActivity(intent);
    }
    public void Settings(View v){
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }
}
